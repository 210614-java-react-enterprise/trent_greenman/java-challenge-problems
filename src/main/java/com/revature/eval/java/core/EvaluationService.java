package main.java.com.revature.eval.java.core;

import java.lang.reflect.Array;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.util.*;

public class EvaluationService {

	/**
	 * 1. Without using the StringBuilder or StringBuffer class, write a method that
	 * reverses a String. Example: reverse("example"); -> "elpmaxe"
	 * 
	 * @param string
	 * @return
	 */
	public String reverse(String string) {
		String reverse = new String();
		for(int i = string.length()-1; i >= 0; i--){
			reverse += string.charAt(i);
		}
		return reverse;
	}

	/**
	 * 2. Convert a phrase to its acronym. Techies love their TLA (Three Letter
	 * Acronyms)! Help generate some jargon by writing a program that converts a
	 * long name like Portable Network Graphics to its acronym (PNG).
	 * 
	 * @param phrase
	 * @return
	 */
	public String acronym(String phrase) {
		// TODO Write an implementation for this method declaration
		String acronym = new String();
		phrase = phrase.toUpperCase();
		String[] split = phrase.split(" ");
		for(String word : split){
			acronym += word.charAt(0);
			//deals with hyphenated phrases
			if(word.contains("-")){
				for(int i=0; i < word.length(); i++){
					if(word.charAt(i) == '-' && i+1 < word.length()){
						acronym += word.charAt(i+1);
					}
				}
			}
		}
		return acronym;
	}

	/**
	 * 3. Determine if a triangle is equilateral, isosceles, or scalene. An
	 * equilateral triangle has all three sides the same length. An isosceles
	 * triangle has at least two sides the same length. (It is sometimes specified
	 * as having exactly two sides the same length, but for the purposes of this
	 * exercise we'll say at least two.) A scalene triangle has all sides of
	 * different lengths.
	 *
	 */
	//static class Triangle {
	public static class Triangle{
		private double sideOne;
		private double sideTwo;
		private double sideThree;

		public Triangle() {
			super();
		}

		public Triangle(double sideOne, double sideTwo, double sideThree) {
			this();
			this.sideOne = sideOne;
			this.sideTwo = sideTwo;
			this.sideThree = sideThree;
		}

		public double getSideOne() {
			return sideOne;
		}

		public void setSideOne(double sideOne) {
			this.sideOne = sideOne;
		}

		public double getSideTwo() {
			return sideTwo;
		}

		public void setSideTwo(double sideTwo) {
			this.sideTwo = sideTwo;
		}

		public double getSideThree() {
			return sideThree;
		}

		public void setSideThree(double sideThree) {
			this.sideThree = sideThree;
		}

		public boolean isEquilateral() {
			// TODO Write an implementation for this method declaration
			if(sideOne == sideTwo && sideOne == sideThree){
				return true;
			} else{
				return false;
			}
		}

		public boolean isIsosceles() {
			// TODO Write an implementation for this method declaration
			if(sideOne == sideTwo || sideOne == sideThree || sideTwo == sideThree){
				return true;
			} else{
				return false;
			}
		}

		public boolean isScalene() {
			// TODO Write an implementation for this method declaration
			if(sideOne != sideTwo && sideOne != sideThree && sideTwo != sideThree){
				return true;
			} else{
				return false;
			}
		}

	}

	/**
	 * 4. Given a word, compute the scrabble score for that word.
	 * 
	 * --Letter Values-- Letter Value A, E, I, O, U, L, N, R, S, T = 1; D, G = 2; B,
	 * C, M, P = 3; F, H, V, W, Y = 4; K = 5; J, X = 8; Q, Z = 10; Examples
	 * "cabbage" should be scored as worth 14 points:
	 * 
	 * 3 points for C, 1 point for A, twice 3 points for B, twice 2 points for G, 1
	 * point for E And to total:
	 * 
	 * 3 + 2*1 + 2*3 + 2 + 1 = 3 + 2 + 6 + 3 = 5 + 9 = 14
	 * 
	 * @param string
	 * @return
	 */
	public int getScrabbleScore(String string) {
		// TODO Write an implementation for this method declaration
		int score = 0;
		string = string.toLowerCase();
		char[] charArray = string.toCharArray();
		String oneString = "aeioulnrst";
		String twoString = "dg";
		String threeString = "bcmp";
		String fourString = "fhvwy";
		String fiveString = "k";
		String eightString = "jx";
		String tenString = "qz";
		for(char i : charArray){
			if(oneString.contains(Character.toString(i))){
				score += 1;
			}else if(twoString.contains(Character.toString(i))){
				score += 2;
			}else if(threeString.contains(Character.toString(i))){
				score += 3;
			}else if(fourString.contains(Character.toString(i))){
				score += 4;
			}else if(fiveString.contains(Character.toString(i))){
				score += 5;
			}else if(eightString.contains(Character.toString(i))){
				score += 8;
			}else if(tenString.contains(Character.toString(i))){
				score += 10;
			}
		}
		return score;
	}

	/**
	 * 5. Clean up user-entered phone numbers so that they can be sent SMS messages.
	 * 
	 * The North American Numbering Plan (NANP) is a telephone numbering system used
	 * by many countries in North America like the United States, Canada or Bermuda.
	 * All NANP-countries share the same international country code: 1.
	 * 
	 * NANP numbers are ten-digit numbers consisting of a three-digit Numbering Plan
	 * Area code, commonly known as area code, followed by a seven-digit local
	 * number. The first three digits of the local number represent the exchange
	 * code, followed by the unique four-digit number which is the subscriber
	 * number.
	 * 
	 * The format is usually represented as
	 * 
	 * 1 (NXX)-NXX-XXXX where N is any digit from 2 through 9 and X is any digit
	 * from 0 through 9.
	 * 
	 * Your task is to clean up differently formatted telephone numbers by removing
	 * punctuation and the country code (1) if present.
	 * 
	 * For example, the inputs
	 * 
	 * +1 (613)-995-0253 613-995-0253 1 613 995 0253 613.995.0253 should all produce
	 * the output
	 * 
	 * 6139950253
	 * 
	 * Note: As this exercise only deals with telephone numbers used in
	 * NANP-countries, only 1 is considered a valid country code.
	 */
	public String cleanPhoneNumber(String string) {
		// TODO Write an implementation for this method declaration
		String numberOnly = new String();
		String legal = "() .-";
		for(int i = 0; i < string.length(); i++){
			if(Character.isDigit(string.charAt(i))){
				numberOnly += string.charAt(i);
			}else if(!legal.contains(Character.toString(string.charAt(i)))){
				throw new IllegalArgumentException();
			}
		}

		if(numberOnly.length() > 11 || numberOnly.length() < 10){
			throw new IllegalArgumentException();
		}else if(numberOnly.length() == 11){
			numberOnly = numberOnly.substring(1);
		}
		return numberOnly;
	}

	/**
	 * 6. Given a phrase, count the occurrences of each word in that phrase.
	 * 
	 * For example for the input "olly olly in come free" olly: 2 in: 1 come: 1
	 * free: 1
	 * 
	 * @param string
	 * @return
	 */
	public Map<String, Integer> wordCount(String string) {
		// TODO Write an implementation for this method declaration
		Map<String, Integer> wordCounter= new HashMap<>();
		String[] phrase = string.split(" |,|\\n");
		for(String i : phrase){
			int count = 0;
			for(String j : phrase){
				if(i.equals(j)){
					count++;
				}
			}
			if(i != ""){
				wordCounter.put(i,count);
			}
		}
		return wordCounter;
	}

	/**
	 * 7. Implement a binary search algorithm.
	 * 
	 * Searching a sorted collection is a common task. A dictionary is a sorted list
	 * of word definitions. Given a word, one can find its definition. A telephone
	 * book is a sorted list of people's names, addresses, and telephone numbers.
	 * Knowing someone's name allows one to quickly find their telephone number and
	 * address.
	 * 
	 * If the list to be searched contains more than a few items (a dozen, say) a
	 * binary search will require far fewer comparisons than a linear search, but it
	 * imposes the requirement that the list be sorted.
	 * 
	 * In computer science, a binary search or half-interval search algorithm finds
	 * the position of a specified input value (the search "key") within an array
	 * sorted by key value.
	 * 
	 * In each step, the algorithm compares the search key value with the key value
	 * of the middle element of the array.
	 * 
	 * If the keys match, then a matching element has been found and its index, or
	 * position, is returned.
	 * 
	 * Otherwise, if the search key is less than the middle element's key, then the
	 * algorithm repeats its action on the sub-array to the left of the middle
	 * element or, if the search key is greater, on the sub-array to the right.
	 * 
	 * If the remaining array to be searched is empty, then the key cannot be found
	 * in the array and a special "not found" indication is returned.
	 * 
	 * A binary search halves the number of items to check with each iteration, so
	 * locating an item (or determining its absence) takes logarithmic time. A
	 * binary search is a dichotomic divide and conquer search algorithm.
	 * 
	 */
	public static class BinarySearch<T extends Comparable>{
		private List<T> sortedList;

		public int indexOf(T t) {
			// TODO Write an implementation for this method declaration
			int index = 0;
			if(sortedList.size()%2 == 1){
				index = sortedList.size() / 2;
			}else{
				index = sortedList.size() / 2 + 1;
			}

			while(sortedList.size() > 0){
				int middle = sortedList.size() / 2;
				if(t.compareTo(sortedList.get(middle)) == 0){
					return index;
				}else if(t.compareTo(sortedList.get(middle)) < 0){
					sortedList = sortedList.subList(0, middle);
					index -= sortedList.size() / 2 + 1;
					//return indexOf(t);
				}else{
					sortedList = sortedList.subList(middle + 1, sortedList.size());
					index += sortedList.size() / 2 + 1;
					//return indexOf(t);
				}
			}

			throw new IllegalArgumentException();
		}

		public BinarySearch(List<T> sortedList) {
			super();
			this.sortedList = sortedList;
		}

		public List<T> getSortedList() {
			return sortedList;
		}

		public void setSortedList(List<T> sortedList) {
			this.sortedList = sortedList;
		}

	}

	/**
	 * 8. Implement a program that translates from English to Pig Latin.
	 * 
	 * Pig Latin is a made-up children's language that's intended to be confusing.
	 * It obeys a few simple rules (below), but when it's spoken quickly it's really
	 * difficult for non-children (and non-native speakers) to understand.
	 * 
	 * Rule 1: If a word begins with a vowel sound, add an "ay" sound to the end of
	 * the word. Rule 2: If a word begins with a consonant sound, move it to the end
	 * of the word, and then add an "ay" sound to the end of the word. There are a
	 * few more rules for edge cases, and there are regional variants too.
	 * 
	 * See http://en.wikipedia.org/wiki/Pig_latin for more details.
	 * 
	 * @param string
	 * @return
	 */
	public String toPigLatin(String string) {
		// TODO Write an implementation for this method declaration
		String vowels = "aeiou";
		String[] words = string.split(" ");
		for(int i=0; i < words.length; i++){
			//starts with a vowel
			if(vowels.contains(Character.toString(words[i].charAt(0)))){
				words[i] = words[i]+"ay";
			}else{
				for(int j=0; j < words[i].length(); j++){
					//goes until we hit a constant or "qu"
					if(Character.toString(words[i].charAt(j)).equals("q") && Character.toString(words[i].charAt(j+1)).equals("u")){
						words[i] = words[i].substring(j+2) + words[i].substring(0,j+2) + "ay";
						break;
					}else if(vowels.contains(Character.toString(words[i].charAt(j)))){
						words[i] = words[i].substring(j) + words[i].substring(0,j) + "ay";
						break;
					}
				}
			}
		}
		String answer = new String();
		for(int i=0; i < words.length; i++){
			if(!(i == words.length-1)){
				answer += words[i] + " ";
			}else{
				answer += words[i];
			}
		}
		return answer;
	}

	/**
	 * 9. An Armstrong number is a number that is the sum of its own digits each
	 * raised to the power of the number of digits.
	 * 
	 * For example:
	 * 
	 * 9 is an Armstrong number, because 9 = 9^1 = 9 10 is not an Armstrong number,
	 * because 10 != 1^2 + 0^2 = 2 153 is an Armstrong number, because: 153 = 1^3 +
	 * 5^3 + 3^3 = 1 + 125 + 27 = 153 154 is not an Armstrong number, because: 154
	 * != 1^3 + 5^3 + 4^3 = 1 + 125 + 64 = 190 Write some code to determine whether
	 * a number is an Armstrong number.
	 * 
	 * @param input
	 * @return
	 */
	public boolean isArmstrongNumber(int input) {
		// TODO Write an implementation for this method declaration
		String num = Integer.toString(input);
		int sum = 0;
		for(int i=0; i < num.length(); i++){
			int digit = Character.getNumericValue(num.charAt(i));
			sum += Math.pow(digit,num.length());
		}
		if(sum == input){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 10. Compute the prime factors of a given natural number.
	 * 
	 * A prime number is only evenly divisible by itself and 1.
	 * 
	 * Note that 1 is not a prime number.
	 * 
	 * @param l
	 * @return
	 */
	public List<Long> calculatePrimeFactorsOf(long l) {
		// TODO Write an implementation for this method declaration
		List<Long> primeFactors = new ArrayList<Long>();
		List<Long> factors = new ArrayList<Long>();
		List<Long> duplicates = new ArrayList<Long>();
		if(l == 2L){
			primeFactors.add(2L);
			return primeFactors;
		}
		for(long i=2L; i<l+1; i++) {
			//Checks if it is a factor
			if (l % i == 0) {
				factors.add(i);
			}
		}
		for(Long factor : factors){
			//Checks if each factor is prime
			boolean isPrime = true;
			for(long j = 2L; j < factor; j++){
				if(factor%j == 0){
					isPrime = false;
				}
			}
			//If a factor is prime, add it to prime factors
			if(isPrime){
				primeFactors.add(factor);
			}
		}
		long input = 1L;
		for(long primeFactor : primeFactors){
			input *= primeFactor;
		}
		//this runs if all prime factors are accounted for
		if(input == l){
			Collections.sort(primeFactors);
			return primeFactors;
		}
		else{
			//recursively deals with duplicate prime factors
			duplicates = calculatePrimeFactorsOf(l/input);
			for(Long duplicate : duplicates){
				primeFactors.add(duplicate);
			}
			Collections.sort(primeFactors);
			return primeFactors;
		}
	}



	/**
	 * 11. Create an implementation of the rotational cipher, also sometimes called
	 * the Caesar cipher.
	 * 
	 * The Caesar cipher is a simple shift cipher that relies on transposing all the
	 * letters in the alphabet using an integer key between 0 and 26. Using a key of
	 * 0 or 26 will always yield the same output due to modular arithmetic. The
	 * letter is shifted for as many values as the value of the key.
	 * 
	 * The general notation for rotational ciphers is ROT + <key>. The most commonly
	 * used rotational cipher is ROT13.
	 * 
	 * A ROT13 on the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: nopqrstuvwxyzabcdefghijklm It is
	 * stronger than the Atbash cipher because it has 27 possible keys, and 25
	 * usable keys.
	 * 
	 * Ciphertext is written out in the same formatting as the input including
	 * spaces and punctuation.
	 * 
	 * Examples: ROT5 omg gives trl ROT0 c gives c ROT26 Cool gives Cool ROT13 The
	 * quick brown fox jumps over the lazy dog. gives Gur dhvpx oebja sbk whzcf bire
	 * gur ynml qbt. ROT13 Gur dhvpx oebja sbk whzcf bire gur ynml qbt. gives The
	 * quick brown fox jumps over the lazy dog.
	 */
	public static class RotationalCipher {
		private int key;

		public RotationalCipher(int key) {
			super();
			this.key = key;
		}

		public String rotate(String string) {
			// TODO Write an implementation for this method declaration
			key = key % 26;
			char[] charArray = string.toCharArray();
			List<Character> newCharArray = new ArrayList<Character>();
			String cipherString = new String();
			for(char character : charArray){
				if(Character.isLetter(character)) {
					int ascii = (int) character;
					ascii += key;
					//deals with ascii values out of range for letters
					if(Character.isLowerCase(character) && ascii > 122){
						ascii -= 26;
					}else if(Character.isUpperCase(character) && ascii > 90){
						ascii -= 26;
					}
					newCharArray.add((char) ascii);
				//if it isn't a letter, don't mess with it
				}else{
					newCharArray.add(character);
				}
			}
			//puts characters into a new string
			for(char character : newCharArray){
				cipherString += character;
			}
			return cipherString;
		}

	}

	/**
	 * 12. Given a number n, determine what the nth prime is.
	 * 
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
	 * that the 6th prime is 13.
	 * 
	 * If your language provides methods in the standard library to deal with prime
	 * numbers, pretend they don't exist and implement them yourself.
	 * 
	 * @param i
	 * @return
	 */
	public int calculateNthPrime(int i) {
		// TODO Write an implementation for this method declaration
		if(i <= 0){
			throw new IllegalArgumentException();
		}
		List<Integer> primes = new ArrayList<Integer>();
		int num = 2;
		//O(n^2) runtime
		while(primes.size() < i){
			boolean isPrime = true;
			for(int j=2; j < num; j++){
				if(num%j == 0){
					isPrime = false;
				}
			}
			if(isPrime){
				primes.add(num);
			}
			num++;
		}
		return primes.get(i-1);
	}

	/**
	 * 13 & 14. Create an implementation of the atbash cipher, an ancient encryption
	 * system created in the Middle East.
	 * 
	 * The Atbash cipher is a simple substitution cipher that relies on transposing
	 * all the letters in the alphabet such that the resulting alphabet is
	 * backwards. The first letter is replaced with the last letter, the second with
	 * the second-last, and so on.
	 * 
	 * An Atbash cipher for the Latin alphabet would be as follows:
	 * 
	 * Plain: abcdefghijklmnopqrstuvwxyz Cipher: zyxwvutsrqponmlkjihgfedcba It is a
	 * very weak cipher because it only has one possible key, and it is a simple
	 * monoalphabetic substitution cipher. However, this may not have been an issue
	 * in the cipher's time.
	 * 
	 * Ciphertext is written out in groups of fixed length, the traditional group
	 * size being 5 letters, and punctuation is excluded. This is to make it harder
	 * to guess things based on word boundaries.
	 * 
	 * Examples Encoding test gives gvhg Decoding gvhg gives test Decoding gsvjf
	 * rxpyi ldmul cqfnk hlevi gsvoz abwlt gives thequickbrownfoxjumpsoverthelazydog
	 *
	 */
	public static class AtbashCipher {

		/**
		 * Question 13
		 * 
		 * @param string
		 * @return
		 */
		public static String encode(String string) {
			// TODO Write an implementation for this method declaration
			string = string.toLowerCase();

			char[] charArray = string.toCharArray();
			List<Character> newCharArray = new ArrayList<Character>();
			String cipherString = new String();

			for(char character : charArray) {
				if (Character.isLetterOrDigit(character)) {
					//converts letter characters to their cipher counterpart
					if (Character.isLetter(character)) {
						int ascii = (int) character - 97;
						ascii = 25 - ascii + 97;
						newCharArray.add((char) ascii);
					}else{
						newCharArray.add(character);
					}

				}
			}
			//puts characters into a new string
			int count = 0;
			for(char character : newCharArray){
				cipherString += character;
				count++;
				//add a space every 5 letters, except at the very end
				if(count % 5 == 0 && count < newCharArray.size()-1){
					cipherString += " ";
				}
			}
			return cipherString;
		}

		/**
		 * Question 14
		 * 
		 * @param string
		 * @return
		 */
		public static String decode(String string) {
			// TODO Write an implementation for this method declaration
			//almost the same as question 13
			string = string.toLowerCase();

			char[] charArray = string.toCharArray();
			List<Character> newCharArray = new ArrayList<Character>();
			String cipherString = new String();

			for(char character : charArray) {
				if (Character.isLetterOrDigit(character)) {
					//converts letter characters to their cipher counterpart
					if (Character.isLetter(character)) {
						int ascii = (int) character - 97;
						ascii = 25 - ascii + 97;
						newCharArray.add((char) ascii);
					}else{
						newCharArray.add(character);
					}

				}
			}

			//puts characters into a new string
			for(char character : newCharArray){
				cipherString += character;
			}
			return cipherString;
		}
	}

	/**
	 * 15. The ISBN-10 verification process is used to validate book identification
	 * numbers. These normally contain dashes and look like: 3-598-21508-8
	 * 
	 * ISBN The ISBN-10 format is 9 digits (0 to 9) plus one check character (either
	 * a digit or an X only). In the case the check character is an X, this
	 * represents the value '10'. These may be communicated with or without hyphens,
	 * and can be checked for their validity by the following formula:
	 * 
	 * (x1 * 10 + x2 * 9 + x3 * 8 + x4 * 7 + x5 * 6 + x6 * 5 + x7 * 4 + x8 * 3 + x9
	 * * 2 + x10 * 1) mod 11 == 0 If the result is 0, then it is a valid ISBN-10,
	 * otherwise it is invalid.
	 * 
	 * Example Let's take the ISBN-10 3-598-21508-8. We plug it in to the formula,
	 * and get:
	 * 
	 * (3 * 10 + 5 * 9 + 9 * 8 + 8 * 7 + 2 * 6 + 1 * 5 + 5 * 4 + 0 * 3 + 8 * 2 + 8 *
	 * 1) mod 11 == 0 Since the result is 0, this proves that our ISBN is valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isValidIsbn(String string) {
		// TODO Write an implementation for this method declaration
		char[] charArray = string.toCharArray();
		List<Character> noDashList = new ArrayList<Character>();
		//get rid of all '-' characters
		for(char character : charArray){
			if(character != '-'){
				noDashList.add(character);
			}
		}

		//add the digits to sum
		int sum = 0;
		for(int i=0; i < 10; i++){
			if(i==9 && noDashList.get(i) == 'X'){
				sum += 10;
			}else if(!Character.isDigit(noDashList.get(i))){
				return false;
			}else{
				sum += (10-i) * Character.getNumericValue(noDashList.get(i));
			}
		}
		return sum % 11 == 0;
	}

	/**
	 * 16. Determine if a sentence is a pangram. A pangram (Greek: παν γράμμα, pan
	 * gramma, "every letter") is a sentence using every letter of the alphabet at
	 * least once. The best known English pangram is:
	 * 
	 * The quick brown fox jumps over the lazy dog.
	 * 
	 * The alphabet used consists of ASCII letters a to z, inclusive, and is case
	 * insensitive. Input will not contain non-ASCII symbols.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isPangram(String string) {
		// TODO Write an implementation for this method declaration
		string = string.toLowerCase();
		char[] charArray = string.toCharArray();
		Set<Character> hashSet = new HashSet<Character>();
		Set<Character> letterSet = new HashSet<Character>(Arrays.asList('a','b','c','d',
				'e','f','g','h','i','j','k','l','m','n','o',
				'p','q','r','s','t','u','v','w','x','y','z'));
		for(char character : charArray){
			if(Character.isLetter(character)){
				hashSet.add(character);
			}
		}
		return hashSet.containsAll(letterSet);
	}

	/**
	 * 17. Calculate the moment when someone has lived for 10^9 seconds.
	 * 
	 * A gigasecond is 109 (1,000,000,000) seconds.
	 * 
	 * @param given
	 * @return
	 */
	public Temporal getGigasecondDate(Temporal given) {
		// TODO Write an implementation for this method declaration
		try{
			LocalDateTime gigaDate = LocalDateTime.from(given);
			gigaDate = gigaDate.plusSeconds(1000000000);
			return gigaDate;
		}catch(java.time.DateTimeException e){
			LocalDate gigaTime = LocalDate.from(given);
			LocalDateTime gigaDate = gigaTime.atStartOfDay();
			gigaDate = gigaDate.plusSeconds(1000000000);
			return gigaDate;
		}
		/*
		LocalDateTime gigaDate = LocalDateTime.from(given);
		//LocalDate gigaTime = LocalDate.from(given);
		//LocalDateTime gigaDate = gigaTime.atStartOfDay();
		System.out.println(gigaDate.toString());
		gigaDate = gigaDate.plusSeconds(1000000000);
		System.out.println(gigaDate.toString());
		//Temporal gigaDate = given.minus(1000000000, ChronoUnit.SECONDS);
		return gigaDate;
		 */
	}

	/**
	 * 18. Given a number, find the sum of all the unique multiples of particular
	 * numbers up to but not including that number.
	 * 
	 * If we list all the natural numbers below 20 that are multiples of 3 or 5, we
	 * get 3, 5, 6, 9, 10, 12, 15, and 18.
	 * 
	 * The sum of these multiples is 78.
	 * 
	 * @param i
	 * @param set
	 * @return
	 */
	public int getSumOfMultiples(int i, int[] set) {
		// TODO Write an implementation for this method declaration
		ArrayList<Integer> multiples = new ArrayList<Integer>();
		// Finds the multiples
		for(int j=2; j < i; j++){
			for(int num : set){
				if(j % num == 0){
					multiples.add(j);
					break;
				}
			}
		}

		int sum = 0;
		//Edge case where 1 is in the set
		for(int multiplier : set){
			if(multiplier == 1){
				sum += 1;
				break;
			}
		}
		//Adds the multiples together
		for(int multiple : multiples){
			sum += multiple;
		}
		return sum;
	}

	/**
	 * 19. Given a number determine whether or not it is valid per the Luhn formula.
	 * 
	 * The Luhn algorithm is a simple checksum formula used to validate a variety of
	 * identification numbers, such as credit card numbers and Canadian Social
	 * Insurance Numbers.
	 * 
	 * The task is to check if a given string is valid.
	 * 
	 * Validating a Number Strings of length 1 or less are not valid. Spaces are
	 * allowed in the input, but they should be stripped before checking. All other
	 * non-digit characters are disallowed.
	 * 
	 * Example 1: valid credit card number 1 4539 1488 0343 6467 The first step of
	 * the Luhn algorithm is to double every second digit, starting from the right.
	 * We will be doubling
	 * 
	 * 4_3_ 1_8_ 0_4_ 6_6_ If doubling the number results in a number greater than 9
	 * then subtract 9 from the product. The results of our doubling:
	 * 
	 * 8569 2478 0383 3437 Then sum all of the digits:
	 * 
	 * 8+5+6+9+2+4+7+8+0+3+8+3+3+4+3+7 = 80 If the sum is evenly divisible by 10,
	 * then the number is valid. This number is valid!
	 * 
	 * Example 2: invalid credit card number 1 8273 1232 7352 0569 Double the second
	 * digits, starting from the right
	 * 
	 * 7253 2262 5312 0539 Sum the digits
	 * 
	 * 7+2+5+3+2+2+6+2+5+3+1+2+0+5+3+9 = 57 57 is not evenly divisible by 10, so
	 * this number is not valid.
	 * 
	 * @param string
	 * @return
	 */
	public boolean isLuhnValid(String string) {
		// TODO Write an implementation for this method declaration
		string = string.replaceAll(" ", "");
		char[] charArray = string.toCharArray();
		int sum = 0;
		for(int i=0; i < charArray.length; i++){
			if(!Character.isDigit(charArray[i])){
				return false;
			}else if(i % 2 == 1){ //Adds doubled numbers
				if(2 * Character.getNumericValue(charArray[charArray.length-1-i]) > 9){
					sum += 2 * Character.getNumericValue(charArray[charArray.length-1-i]) - 9;
				}else{
					sum += 2 * Character.getNumericValue(charArray[charArray.length-1-i]);
				}
			}else{ //Adds non doubled numbers
				sum += Character.getNumericValue(charArray[charArray.length-1-i]);
			}
		}
		if(sum % 10 == 0){
			return true;
		}else{
			return false;
		}
	}

	/**
	 * 20. Parse and evaluate simple math word problems returning the answer as an
	 * integer.
	 * 
	 * Add two numbers together.
	 * 
	 * What is 5 plus 13?
	 * 
	 * 18
	 * 
	 * Now, perform the other three operations.
	 * 
	 * What is 7 minus 5?
	 * 
	 * 2
	 * 
	 * What is 6 multiplied by 4?
	 * 
	 * 24
	 * 
	 * What is 25 divided by 5?
	 * 
	 * 5
	 * 
	 * @param string
	 * @return
	 */
	public int solveWordProblem(String string) {
		// TODO Write an implementation for this method declaration
		string = string.replaceAll("\\?","");
		String[] words = string.split(" ");
		int problemType = -1;
		Integer num1 = null;
		Integer num2 = null;
		for(String word : words){
			if(num1 == null){
				try{
					num1 = Integer.parseInt(word);
				}catch(NumberFormatException e){}
			}else if(num2 == null){
				try{
					num2 = Integer.parseInt(word);
				}catch(NumberFormatException e){}
			}

			if(word.equals("plus")){
				problemType = 0;
			}else if(word.equals("minus")){
				problemType = 1;
			}else if(word.equals("multiplied")){
				problemType = 2;
			}else if(word.equals("divided")){
				problemType = 3;
			}
		}
		
		switch(problemType){
			case 0:
				return num1 + num2;
			case 1:
				return num1 - num2;
			case 2:
				return num1 * num2;
			case 3:
				return num1 / num2;
		}
		return 0;
	}

}
